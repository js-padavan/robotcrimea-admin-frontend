import { Route } from '@angular/router';
import { PaymentComponent } from './containers/payment/payment.component';

export const routes: Route[] = [
    { path: '', component: PaymentComponent }
]