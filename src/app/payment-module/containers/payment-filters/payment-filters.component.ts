import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DEFAULT_FILTERS_FORM } from '@services/models/payment.model';
import { cloneDeep as _cloneDeep } from 'lodash';
import { ISelectOption } from '@services/models/statistic.model';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'payment-filters',
    templateUrl: './payment-filters.component.html',
    styleUrls: ['./payment-filters.component.scss']
})

export class PaymentFiltersComponent {
    @Input()
    public statusSelectOpts: ISelectOption[] = [];
    @Input()
    public typeSelectOpts: ISelectOption[] = [];
    @Output()
    public submitFilters = new EventEmitter<any>()

    public filtersForm = _cloneDeep(DEFAULT_FILTERS_FORM)

    constructor(
    ) {
    }

    filterPayments() {
        this.submitFilters.emit(this.filtersForm)
    }

    clearFilters() {
        this.filtersForm = _cloneDeep(DEFAULT_FILTERS_FORM);
        this.filterPayments()
    }
}