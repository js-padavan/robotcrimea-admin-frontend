import { Component } from "@angular/core";
import { PaymentRestClient } from '@services/resources/PaymentRestClient.service';
import { PageEvent } from '@angular/material/paginator';
import { IPayment, PAYMENT_STATUS_LABELS, PAYMENT_TYPE_LABELS } from '@services/models/payment.model';
import { ISelectOption } from '@services/models/statistic.model';

@Component({
    selector: 'payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})

export class PaymentComponent {
    public paymentList: IPayment[] = [];
    public displayedColumns: string[] = ['createdBy', 'student', 'amount', 'groupName', 'createdAt', 'status', 'type', 'invoiceId', 'transactionId'];
    public pageEvent: PageEvent;
    public STATUS_LABELS = PAYMENT_STATUS_LABELS;
    public TYPE_LABELS = PAYMENT_TYPE_LABELS;
    public statusSelectOpts: ISelectOption[] = [];
    public typeSelectOpts: ISelectOption[] = [];
    public isLoading: boolean;

    public pageOptions = {
        page: 1,
        pageSize: 50,
        totalDocs: 1
    }
    constructor(
        private paymentRestClient: PaymentRestClient
    ){}
    
    ngOnInit() {
        this.isLoading = true;
        this.statusSelectOpts = Object.values(PAYMENT_STATUS_LABELS);
        this.typeSelectOpts = Object.values(PAYMENT_TYPE_LABELS);
        this.updateList()
    }

    async updateList(filters?) {
        this.isLoading = true;
        let response = await this.paymentRestClient.query(filters, this.pageOptions.page, this.pageOptions.pageSize);
        this.paymentList = response.docs;
        this.pageOptions.totalDocs = response.totalDocs;
        this.pageOptions.pageSize = response.limit
        this.isLoading = false;
    }

    changePage(event: PageEvent) {
        this.pageOptions.page = event.pageIndex + 1;
        this.pageOptions.pageSize = event.pageSize;
        this.updateList();
        return event
    }

    filterPayments(form: any) {  
        this.updateList(form)
    }
}
