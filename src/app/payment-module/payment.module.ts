import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PaymentComponent } from './containers/payment/payment.component';
import { PaymentFiltersComponent } from './containers/payment-filters/payment-filters.component';
import { BadgeModule } from '@ui/badge';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        InlineSVGModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDialogModule,
        MatTableModule,
        MatSelectModule,
        MatPaginatorModule,
        BadgeModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRippleModule,
        MatIconModule,
        MatProgressSpinnerModule
    ],
    declarations: [
        PaymentComponent,
        PaymentFiltersComponent
    ]
})
export class PaymentModule {

}