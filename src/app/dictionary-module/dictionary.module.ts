import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BranchOfficeComponent } from './containers/branch-office/branch-office.component';
import { MatButtonModule } from '@angular/material/button';
import { AddModalComponent } from './containers/add-modal/add-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        InlineSVGModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDialogModule,
        MatTableModule,
        MatSelectModule,
        MatPaginatorModule
    ],
    declarations: [
        BranchOfficeComponent,
        AddModalComponent
    ],
    providers: [
    ]
})
export class DictionaryModule {

}