import { Route } from '@angular/router';
import { BranchOfficeComponent } from './containers/branch-office/branch-office.component';

export const routes: Route[] = [
    { path: 'branch-office', component: BranchOfficeComponent }
]