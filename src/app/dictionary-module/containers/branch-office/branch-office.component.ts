import { Component } from '@angular/core';
import { BranchOfficeRestClient } from '@services/resources/BranchOfficeRestClient.service';
import { MatDialog } from '@angular/material/dialog';
import { AddModalComponent } from '../add-modal/add-modal.component';
import { IDictionaryTable, IBranchOffice } from '@services/models/dictionary.model';
import { PageEvent } from '@angular/material/paginator';

@Component({
    selector: 'branch-office',
    templateUrl: './branch-office.component.html',
    styleUrls: ['./branch-office.component.scss']
})

export class BranchOfficeComponent {
    public officeList: IBranchOffice[] = [];
    public displayedColumns: string[] = ['name', 'address', 'id', 'createdAt', 'button'];
    public pageEvent: PageEvent;
    public pageOptions = {
        page: 1,
        pageSize: 50,
        totalDocs: 1
    }

    constructor(
        public dialog: MatDialog,
        private officeRestClient: BranchOfficeRestClient
    ) {}

    ngOnInit() {
        this.updateList();
    }

    async updateList() {
        let response = await this.officeRestClient.query(null, this.pageOptions.page, this.pageOptions.pageSize);
        this.officeList = response.docs;
        this.pageOptions.totalDocs = response.totalDocs;
        this.pageOptions.pageSize = response.limit
    }

    openAddModal() {
        const dialogRef = this.dialog.open(AddModalComponent, {
            width: '700px',
            data: {address: '', name: ''}
          });
      
          dialogRef.afterClosed().subscribe(result => {
            this.officeRestClient.save({name: result.name, address: result.address})
                .then(() => this.updateList())
          });
    }

    removeItem(id: string) {
        this.officeRestClient.delete(id);
        this.updateList();
    }

    changePage(event: PageEvent) {
        this.pageOptions.page = event.pageIndex + 1;
        this.pageOptions.pageSize = event.pageSize;
        this.updateList();
        return event
    }
}