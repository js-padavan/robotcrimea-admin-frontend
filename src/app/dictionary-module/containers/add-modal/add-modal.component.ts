import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IBranchOfficePostForm } from '@services/models/dictionary.model';

@Component({
    selector: 'add-modal',
    templateUrl: './add-modal.component.html',
    styleUrls: ['./add-modal.component.scss']
})

export class AddModalComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<AddModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IBranchOfficePostForm
    ) {}

    async ngOnInit() {
      
    }

}