import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { AdminLayoutComponent } from './containers/admin-layout.component';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { SideNavComponent } from './containers/side-nav/side-nav.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        InlineSVGModule,
        MatMenuModule,
        MatButtonModule
    ],
    declarations: [
        AdminLayoutComponent,
        SideNavComponent
    ]
})
export class AdminLayoutModule {

}