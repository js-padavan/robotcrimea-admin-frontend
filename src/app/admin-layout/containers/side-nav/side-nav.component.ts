import { Component } from "@angular/core"
import { UserService } from '@services/Users.service';

@Component({
    selector: 'side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.scss']
})

export class SideNavComponent {
    constructor(
        public userService: UserService
    ) {}
}