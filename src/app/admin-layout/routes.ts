import { Route } from '@angular/router';
import { AdminLayoutComponent } from './containers/admin-layout.component';
import { AuthGuard } from '@services/AuthGuard.service';

export const routes: Route[] = [
    {path: '', canActivate: [AuthGuard], component: AdminLayoutComponent, children: [
    ]}
]