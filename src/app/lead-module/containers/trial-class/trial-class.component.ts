import { Component, OnInit } from "@angular/core"
import { TrialClassRestClient } from '@services/resources/TrialClassRestClient.service';
import { MatDialog } from '@angular/material/dialog';
import { TrialClassModalComponent } from '../trial-class-modal/trial-class-modal.component';
import { DateTime } from 'luxon';
import { ITrialClass } from '@services/models/lead.model';
import { DatePipe } from '@angular/common';
import { PageEvent } from '@angular/material/paginator';

@Component({
    selector: 'trial-class',
    templateUrl: './trial-class.component.html',
    styleUrls: ['./trial-class.component.scss']
})

export class TrialClassComponent implements OnInit {
    public trialClassList: ITrialClass[] = []
    public displayedColumns: string[] = ['date', 'mentor', 'office', 'createdAt', 'button'];
    private pipe = new DatePipe('en-US');
    public pageEvent: PageEvent;
    public isLoading: boolean;
    public isActiveTab: number = 2;
    public pageOptions = {
        page: 1,
        pageSize: 50,
        totalDocs: 1
    }
    constructor(
        public dialog: MatDialog,
        private trialClassRestClient: TrialClassRestClient,
    ) {
    }
    
    async ngOnInit() {
        this.updateClassList()
    }

    openAddModal() {
        const dialogRef = this.dialog.open(TrialClassModalComponent, {
            width: '400px',
            data: {mentor: '', office: '', date: '', time: ''}
          });
      
          dialogRef.componentInstance.submitForm.subscribe((data) => {
            this.trialClassRestClient.save({
                    mentor: data.mentor,
                    office: data.office,
                    date: this.transformTime(data)
                })
                .then(() => {
                    this.updateClassList();
                    dialogRef.componentInstance.isLoading = false;
                    dialogRef.close()
                })
          });
    }

    openUpdateModal(trialClass: ITrialClass) {
        const dialogRef = this.dialog.open(TrialClassModalComponent, {
            width: '400px',
            data: {
                mentor: trialClass.mentor._id,
                office: trialClass.office._id,
                date: trialClass.date, 
                time: this.pipe.transform(trialClass.date, 'HH:mm')
            }
        });
        dialogRef.componentInstance.submitForm.subscribe((data) => {
            this.trialClassRestClient.update(trialClass.id, {
                mentor: data.mentor,
                date: this.transformTime(data),
                office: data.office
            }).then(() => {
                this.updateClassList();
                dialogRef.componentInstance.isSaving = false;
                dialogRef.close()
            })
        })
    }

    transformTime(form) {
        let date: any;
        if(typeof form.date === 'string') {
            date = DateTime.fromISO(form.date).toJSDate()
        } else {
            date = form.date
        }
        let time = DateTime.fromString(form.time, 'HH:mm');
        let fullDate = DateTime.fromISO(DateTime.fromJSDate(date).toISO()) 
            .set({ hour: time.hour, minute: time.minute }).toISO();
        return fullDate
    }

    async updateClassList(filters?) {
        this.isLoading = true;
        let response = await this.trialClassRestClient.query(filters, this.pageOptions.page, this.pageOptions.pageSize);
        this.trialClassList = response.docs;
        this.pageOptions.totalDocs = response.totalDocs;
        this.pageOptions.pageSize = response.limit;
        this.isLoading = false;
    }

    async removeItem(id: string) {
        await this.trialClassRestClient.delete(id);
        this.updateClassList();
    }

    changePage(event: PageEvent) {
        this.pageOptions.page = event.pageIndex + 1;
        this.pageOptions.pageSize = event.pageSize;
        this.updateClassList();
        return event
    }

    activateTab(index: number) {
        this.isActiveTab = index;
        if(index == 0) {
            this.updateClassList(null)
        } else if(index == 1) {
            this.updateClassList({pastDate: new Date()})
        } else if(index == 2) {
            this.updateClassList({date: new Date()})
        }
    }
}