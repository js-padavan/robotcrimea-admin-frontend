import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DEFAULT_FILTERS_FORM } from '@services/models/lead.model';
import { cloneDeep as _cloneDeep } from 'lodash';
import { ISelectOption } from '@services/models/statistic.model';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'lead-filters',
    templateUrl: './lead-filters.component.html',
    styleUrls: ['./lead-filters.component.scss']
})

export class LeadFiltersComponent {
    @Input()
    public statusSelectOpts: ISelectOption[] = [];
    @Input()
    public officeSelectOpts: ISelectOption[] = [];
    @Output()
    public submitFilters = new EventEmitter<any>()

    public filtersForm = _cloneDeep(DEFAULT_FILTERS_FORM)

    constructor(

    ) {
    }

    filterLeads() {
        this.submitFilters.emit(this.filtersForm)
    }

    clearFilters() {
        this.filtersForm = _cloneDeep(DEFAULT_FILTERS_FORM);
        this.filterLeads()
    }
}