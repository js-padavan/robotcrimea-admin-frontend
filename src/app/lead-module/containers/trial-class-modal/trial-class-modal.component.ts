import { Component, Inject, Output, EventEmitter, Input } from "@angular/core";
import { ISelectOption } from '@services/models/statistic.model';
import { UsersRestClient } from '@services/resources/UsersRestClient.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ITrialClassPostForm } from '@services/models/lead.model';
import { NgForm } from '@angular/forms';
import { BranchOfficeRestClient } from '@services/resources/BranchOfficeRestClient.service';

@Component({
    selector: 'trial-class-modal',
    templateUrl: './trial-class-modal.component.html',
    styleUrls: ['./trial-class-modal.component.scss']
})

export class TrialClassModalComponent {
    
    @Input()
    public isSaving: boolean = false;

    @Output()
    public submitForm = new EventEmitter<ITrialClassPostForm>();

    public mentorsSelectOpts: ISelectOption[] = [];
    public officeSelectOpts: ISelectOption[] = [];
    public timeFormat: number = 24;
    public isLoading: boolean = true;

    constructor(
        private usersRestClient: UsersRestClient,
        private officeRC: BranchOfficeRestClient,
        public dialogRef: MatDialogRef<TrialClassModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ITrialClassPostForm
    ) {}

    async ngOnInit() {
        let [users, offices] = await Promise.all([
            await this.usersRestClient.query({role: ['ROLE_MENTOR', 'ROLE_ADMIN']}, null, null),
            await this.officeRC.query({}, 0, 1)
        ]);
        this.mentorsSelectOpts = users.docs.map( item => ({value: item._id, label: `${item.name} ${item.lastName}`}));
        this.officeSelectOpts = offices.docs.map( item => ({value: item._id, label: item.name}));
        this.isLoading = false;
    }

    submit(ngForm: NgForm) {
        if(!ngForm.valid) {
            return
        }
        this.isSaving = true;
        this.submitForm.emit(this.data);
    }
}