import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { MatDialogRef } from '@angular/material/dialog';
import { TrialClassRestClient } from '@services/resources/TrialClassRestClient.service';
import { ISelectOption } from '@services/models/statistic.model';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'add-class-modal',
    templateUrl: './add-class-modal.component.html',
    styleUrls: ['./add-class-modal.component.scss']
})

export class AddClassModalComponent implements OnInit {
    @Input()
    public isLoading: boolean = false;
    @Output()
    public submitForm = new EventEmitter<string>();
    public trialClassSelectOpts: ISelectOption[] = [];
    public selectedTrialClass: string;

    constructor(
        private trialClassRC: TrialClassRestClient,
        public dialogRef: MatDialogRef<AddClassModalComponent>,
    ) {}

    async ngOnInit() {
        let classes = await this.trialClassRC.query({date: new Date()}, 0, 1);
        this.trialClassSelectOpts = classes.docs.map( item => ({value: item._id, label: item.date}));
        console.log(this);
    }

    submit(ngForm: NgForm) {
        if(!ngForm.valid) {
            return
        }
        this.isLoading = true;
        this.submitForm.emit(this.selectedTrialClass);
    }
}