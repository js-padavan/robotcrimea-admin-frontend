import { Component, Inject, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { ISelectOption } from '@services/models/statistic.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ILeadPostForm, LEAD_STATUS_LABELS } from '@services/models/lead.model';
import { CityRestClient } from '@services/resources/CityRestClient.service';
import { NgForm } from '@angular/forms';
import { TrialClassRestClient } from '@services/resources/TrialClassRestClient.service';
import { BranchOfficeRestClient } from '@services/resources/BranchOfficeRestClient.service';

@Component({
    selector: 'lead-modal',
    templateUrl: './lead-modal.component.html',
    styleUrls: ['./lead-modal.component.scss']
})

export class LeadModalComponent implements OnInit {
    @Input()
    public isLoading: boolean = false;
    
    @Output()
    public submitForm = new EventEmitter<ILeadPostForm>();

    public citySelectOpts: ISelectOption[] = [];
    public statusSelectOpts: ISelectOption[] = [];
    public trialClassSelectOpts: ISelectOption[] = [];
    public officeSelectOpts: ISelectOption[] = [];
    constructor(
        private trialClassRC: TrialClassRestClient,
        private officeRC: BranchOfficeRestClient,
        public dialogRef: MatDialogRef<LeadModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ILeadPostForm
    ) {}

    async ngOnInit() {
        let offices = await this.officeRC.query(null, 0, 1);
        this.officeSelectOpts = offices.docs.map( item => ({value: item._id, label: item.name}));
        this.statusSelectOpts = Object.values(LEAD_STATUS_LABELS);
        let classes = await this.trialClassRC.query({date: new Date()}, 0, 1)
        this.trialClassSelectOpts = classes.docs.map( item => ({value: item._id, label: item.date}));
    }

    submit(ngForm: NgForm) {
        if(!ngForm.valid) {
            return
        }
        this.isLoading = true;
        this.submitForm.emit(this.data);
    }
}