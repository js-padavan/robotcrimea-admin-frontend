import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DIFFRENCE_POINTS_LABELS } from '@services/models/lead.model';

@Component({
    selector: 'lead-history-modal',
    templateUrl: './lead-history-modal.component.html',
    styleUrls: ['./lead-history-modal.component.scss']
})

export class LeadHistoryModalComponent {
    
    public pointLabels = DIFFRENCE_POINTS_LABELS;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}
}