import { Component, ViewChild, ElementRef } from "@angular/core"
import { MatDialog } from '@angular/material/dialog';
import { LeadModalComponent } from '../lead-modal/lead-modal.component';
import { LeadHistoryModalComponent } from '../lead-history-modal/lead-history-modal.component';
import { LeadRestClient, sortOptions } from '@services/resources/LeadRestClient.service';
import { DateTime } from 'luxon';
import { DEFAULT_LEAD_FORM, ILead, ILeadPostForm, LEAD_STATUS_LABELS } from '@services/models/lead.model';
import { IUploadTracker } from 'src/app/ui/dropzone/uploadTrackerManager.service';
import { IUploadConfig } from 'src/app/ui/dropzone';
import { FileUploaderFactory } from '@services/FileUploaderFactory.service';
import { MentorStatService } from '@services/MentorStats.service';
import { ISelectOption } from '@services/models/statistic.model';
import { ToastrService } from 'ngx-toastr';
import { BranchOfficeRestClient } from '@services/resources/BranchOfficeRestClient.service';
import { AddClassModalComponent } from '../add-class-modal/add-class-modal.component';
import { PageEvent } from '@angular/material/paginator';
import { UserService } from '@services/Users.service';

@Component({
    selector: 'lead',
    templateUrl: './lead.component.html',
    styleUrls: ['./lead.component.scss']
})

export class LeadComponent {

    @ViewChild('dzcontainer', { read: ElementRef, static: true })
    private _dzcontainer: ElementRef;
    public statusSelectOpts: ISelectOption[] = [];
    public officeSelectOpts: ISelectOption[] = [];
    public STATUS_LABELS = LEAD_STATUS_LABELS;
    public docUploadConfig: IUploadConfig;
    public leadList: ILead[] = [];
    public isLoading: boolean;
    public displayedColumns: string[] = [
        'checkbox',
        'createdAt',
        'phone',
        'parentFIO',
        'childBirthday',
        'id',
        'status',
        'comment',
        'office',
        'date',
        'button'
    ];
    isUploading: boolean = false;
    public checkboxValues: boolean[] = [];   
    public pageEvent: PageEvent;
    public pageOptions = {
        page: 1,
        pageSize: 50,
        totalDocs: 1
    }

    public sortOption = null;
    public sortSelectOpts: ISelectOption[] = sortOptions;

    constructor(
        public dialog: MatDialog,
        private leadRestClient: LeadRestClient,
        private _fileUploaderFactory: FileUploaderFactory,
        private _mentorService: MentorStatService,
        public toastr: ToastrService,
        private officeRC: BranchOfficeRestClient,
        public userService: UserService
    ) {
        this.docUploadConfig = this._fileUploaderFactory.getFileUploader()
    }


    async ngOnInit() {
        this.isLoading = true;
        this.statusSelectOpts = Object.values(LEAD_STATUS_LABELS);
        let offices = await this.officeRC.query({}, 0, 1);
        this.officeSelectOpts = offices.docs.map( item => ({value: item._id, label: item.name}));
        this.updateLeadList()
    }    

    openAddModal() {
        const dialogRef = this.dialog.open(LeadModalComponent, {
            width: '600px',
            data: DEFAULT_LEAD_FORM
          });
          dialogRef.componentInstance.submitForm.subscribe((data: ILeadPostForm) => {
            data.childBirthday = (typeof data.childBirthday === 'string')? DateTime.fromISO(data.childBirthday).toISO() : DateTime.fromJSDate(data.childBirthday).toISO();
            this.leadRestClient.save(data)
                .then(() => {
                    this.updateLeadList();
                    dialogRef.componentInstance.isLoading = false;
                    dialogRef.close();
                })
          })
    }

    openUpdateModal(item: ILead) {
        const dialogRef = this.dialog.open(LeadModalComponent, {
            width: '600px',
            data: this.prepareObject(item)
        });
        dialogRef.componentInstance.submitForm.subscribe((form: ILeadPostForm) => {
            form.childBirthday = (typeof form.childBirthday === 'string')? DateTime.fromISO(form.childBirthday).toISO() : DateTime.fromJSDate(form.childBirthday).toISO();
            this.leadRestClient.update(item.id, form)
                .then(() => {
                    this.updateLeadList()
                    dialogRef.componentInstance.isLoading = false;
                    dialogRef.close()
                })
        })
    }

    openClassModal() {
        if( !Object.values(this.checkboxValues).find(item => item == true)) {
            this.toastr.error('Выберите хотябы одного лида');
            return
        }
        const dialogRef = this.dialog.open(AddClassModalComponent, {
            width: '600px',
        });
        dialogRef.componentInstance.submitForm.subscribe((trialClassId) => {
            this.leadList.forEach((item) => {
                if(this.checkboxValues[item.id]) {
                    let form = this.prepareObject(item);
                    form.trialClass = trialClassId;
                    this.leadRestClient.update(item.id, form)
                        .then(() => {
                            this.updateLeadList();
                            dialogRef.componentInstance.isLoading = false;
                            dialogRef.close();
                        });
                }
            })
        })
    }

    prepareObject(lead: ILead): ILeadPostForm {
        return {
            parentFIO: lead.parentFIO,
            childFIO: lead.childFIO,
            childBirthday: (typeof lead.childBirthday === 'string')? DateTime.fromISO(lead.childBirthday).toISO() : DateTime.fromJSDate(lead.childBirthday).toISO(), 
            courseName: lead.courseName,
            phone: lead.phone,
            branchOffice: lead.branchOffice?._id,
            status: lead.status,
            comment: lead.comment,
            trialClass: lead.trialClass?._id
        }
    }

    async updateLeadList(filters?) {
        this.isLoading = true;
        let response = await this.leadRestClient.query(filters, this.pageOptions.page, this.pageOptions.pageSize, this.sortOption);
        this.leadList = response.docs;
        this.pageOptions.totalDocs = response.totalDocs;
        this.pageOptions.pageSize = response.limit
        this.leadList.forEach((item) => {
            this.checkboxValues[item.id] = false
        })
        this.isLoading = false;
    }

    async removeItem(id: string) {
        await this.leadRestClient.delete(id);
        this.updateLeadList();
    }

    onDzClick($event: MouseEvent) {
        if ($event.target === this._dzcontainer.nativeElement ) {
          return;
        }
        this._dzcontainer.nativeElement.click();
    }

    public addFiles(file: IUploadTracker) {
        this.isUploading = true;
    }

    public onUploadSuccess(data: any) {
        if(data.error) {
            this.toastr.error(data.message);
        } else {
            this.toastr.success('Успешно загружен');
            this.updateLeadList();
        }
        this.isUploading = false;
    }

    showHistory(id: string) {
        this._mentorService.loadMentorHistory(id)
            .then((response) => {
                let storyPoints = [];
                response.forEach((item) => {
                    storyPoints.push({
                        user: item.user,
                        date: item.createdAt,
                        diffKeys: Object.keys(item.diff),
                        diffrence: Object.values(item.diff)
                    })
                    
                })
                const dialogRef = this.dialog.open(LeadHistoryModalComponent, {
                    width: '600px',
                    data: storyPoints
                });
            })
    }

    filterLeads(form: any) {  
        this.updateLeadList(form)
    }

    changeStatus(status: string, item: ILead) {
        let lead = item;
        lead.status = status;
        this.leadRestClient.update(item.id, this.prepareObject(lead))
            .then(() => this.updateLeadList())
    }

    changePage(event: PageEvent) {
        this.pageOptions.page = event.pageIndex + 1;
        this.pageOptions.pageSize = event.pageSize;
        this.updateLeadList();
        return event
    }

    sortLeads() {
        this.updateLeadList()
    }
}