import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { LeadComponent } from './containers/lead/lead.component';
import { LeadModalComponent } from './containers/lead-modal/lead-modal.component';
import { TrialClassComponent } from './containers/trial-class/trial-class.component';
import { TrialClassModalComponent } from './containers/trial-class-modal/trial-class-modal.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule, MAT_DATE_LOCALE, MatPseudoCheckboxModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { DropzoneModule } from '@ui/dropzone';
import { BadgeModule } from '@ui/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { LeadHistoryModalComponent } from './containers/lead-history-modal/lead-history-modal.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LeadFiltersComponent } from './containers/lead-filters/lead-filters.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AddClassModalComponent } from './containers/add-class-modal/add-class-modal.component';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        InlineSVGModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDialogModule,
        MatTableModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRippleModule,
        NgxMaterialTimepickerModule,
        DropzoneModule,
        MatMenuModule,
        MatIconModule,
        MatProgressSpinnerModule,
        BadgeModule,
        MatCheckboxModule,
        MatPaginatorModule
    ],
    declarations: [
        LeadComponent,
        TrialClassComponent,
        TrialClassModalComponent,
        LeadModalComponent,
        LeadHistoryModalComponent,
        LeadFiltersComponent,
        AddClassModalComponent
    ],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
    ]
})
export class LeadModule {

}