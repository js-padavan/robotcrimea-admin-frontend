import { Route } from '@angular/router';
import { LeadComponent } from './containers/lead/lead.component';
import { TrialClassComponent } from './containers/trial-class/trial-class.component';

export const routes: Route[] = [
    { path: 'lead', component: LeadComponent },
    { path: 'trial-class', component: TrialClassComponent }
]