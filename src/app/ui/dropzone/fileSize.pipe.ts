import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'fileSize'})
export class FileSizePipe implements PipeTransform {

  private units = [
    'bytes',
    'kb',
    'mb',
    'gb',
    'tb',
  ];

  transform(bytes: number = 0, precision: number = 2 , noSuffix = false) : string {
    if ( isNaN( parseFloat( String(bytes) )) || ! isFinite( bytes ) ) return '?';

    let unit = 0;

    while ( bytes >= 1024 ) {
      bytes /= 1024;
      unit ++;
    }

    let size = bytes.toFixed( + precision );

    return noSuffix ? size : size  + ' ' + this.units[ unit ];
  }
}