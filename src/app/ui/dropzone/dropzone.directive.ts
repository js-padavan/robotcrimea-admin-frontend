import { Directive, ElementRef, Inject, PLATFORM_ID, NgZone, Input, EventEmitter, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import * as Dropzone from 'dropzone';
import { IUploadTracker, IUploadTrackerHandler, UploadTrackerManager, IUploadResponse } from './uploadTrackerManager.service';
import { IUploadConfig } from './interfaces/uploadConfig.interface';
import { isEmpty as _isEmpty } from 'lodash';

@Directive({
    selector: '[dropzone]',
})
export class DropzoneDirective {
    @Input('dzConfig')
    public config: IUploadConfig;

    @Output('dzUploadStarted')
    public onUploadStart = new EventEmitter<IUploadTracker>();

    @Output('dzUploadSuccess')
    public onUploadSuccess = new EventEmitter<IUploadResponse>();

    @Output('dzFileOver')
    public onFileOver = new EventEmitter<boolean>();


    public dropzone: Dropzone;
    public dragEnteredEls: EventTarget[] = [];
    private _uploadTrackerHadnler: IUploadTrackerHandler = null;

    constructor(
        private _elRef: ElementRef,
        @Inject(PLATFORM_ID) private _platformId: Object,
        private _zone: NgZone,
        private _uploadTrackerManager: UploadTrackerManager
    ) {
    }

    ngAfterViewInit() {
        if (!isPlatformBrowser(this._platformId)) {
            return;
        }

        this._zone.runOutsideAngular(() => {
            this.dropzone = new Dropzone(this._elRef.nativeElement, {
                chunking: true,
                url: '/api/v2/*', // TBA later
                uploadMultiple: false,
                previewTemplate: '',
                clickable: true,
                headers: {
                    Authorization:'Bearer ' + window.localStorage.getItem('robotcrimea-token')
                },
                addedfile: () => {},
                parallelUploads: 1, // очень важно, иначе у нас трекеры аплоадинга перемешаются
                createImageThumbnails: false,
                acceptedFiles: this.config.acceptedFiles,
                maxFilesize: this.config.maxFileSize ? this.config.maxFileSize : 256 // по дефолту ограничиваем в 256mb     
            });
            this.dropzone.on('processing', (file: Dropzone.DropzoneFile) => {
                // в чате можно аплоадить все что угодно, поэтому надо в рантайме определять эндпоинт для аплоада
                // https://github.com/enyo/dropzone/wiki/Set-URL-dynamically
                (this.dropzone as any).options.url = this.config.url(file)
                this._uploadTrackerHadnler = this._uploadTrackerManager.createUploadTracker(file, () => this.dropzone.removeAllFiles(true));
                this._zone.run(() => {
                    this.onUploadStart.emit(
                        this._uploadTrackerManager.getTrackerByUid(this._uploadTrackerHadnler.uid)
                    )
                    this.onFileOver.emit(false);
                    this.dragEnteredEls.length = 0;
                })
            });
            this.dropzone.on('success', (file: Dropzone.DropzoneFile, response: IUploadResponse, e) => {
                this._zone.run(() => {
                    let response: IUploadResponse = JSON.parse(file.xhr.response);
                    this._uploadTrackerHadnler.progress.complete();
                    this._uploadTrackerHadnler.success.next(response);
                    this._uploadTrackerHadnler.success.complete();
                    this.onUploadSuccess.emit(response);
                    this._uploadTrackerHadnler = null;
                })
            });
            this.dropzone.on('error', (file: Dropzone.DropzoneFile, response: any, e) => {
                this.onUploadSuccess.emit(response);
            }) 
            this.dropzone.on('uploadprogress', (file: File, progress: number, bytesSent: number) => {
                this._zone.run(() => {
                    this._uploadTrackerHadnler.progress.next({
                        percent: bytesSent / file.size * 100,
                        bytesSent: bytesSent
                    })
                })
            });
            this.dropzone.on('canceled', () => {
                this._zone.run(() => {
                    this._uploadTrackerHadnler.onCancel.next();
                    this._uploadTrackerHadnler.onCancel.complete();
                    this._uploadTrackerHadnler.progress.complete();
                    this._uploadTrackerHadnler.success.complete();
                })
            })
            this.dropzone.on('dragenter', (e) => {
                this.dragEnteredEls.push(e.target);
                this._zone.run(() => {
                    this.onFileOver.emit(true);
                })
            });
            this.dropzone.on('dragleave', (e) => {
                this.dragEnteredEls = this.dragEnteredEls.filter(item => item !== e.target);
                if (this.dragEnteredEls.length === 0) {
                    this._zone.run(() => {
                        this.onFileOver.emit(false);
                    })
                }
            })
        })
    }

    ngOnDestroy() {
        if (!this._uploadTrackerHadnler) {
            this._destroy();
        } else {
            this._uploadTrackerHadnler.success.subscribe(null, null, () => {
                this._destroy();
            })
        }
    }

    private _destroy() {
        this._zone.runOutsideAngular(() => {
            this.dropzone && this.dropzone.destroy();
        })
    }
}