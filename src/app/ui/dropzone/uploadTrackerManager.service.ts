import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject, Subject } from "rxjs";

export interface IUploadResponse {
    path: string;
    thumbnailPath?: string;
    videoDuration?: number;
    // videoConversionStatus?:VIDEO_CONVERSION_STATUS;
    height?: number;
    width?: number;
}

export interface IUploadProgress {
    percent: number;
    bytesSent: number;
}

export interface IUploadTracker {
    progress: Observable<IUploadProgress>;
    success: Observable<IUploadResponse>;
    onCancel: Observable<void>;
    file: Dropzone.DropzoneFile;
    uid: string;
    abort: () => void
}

export interface IUploadTrackerHandler {
    uid: string;
    progress: BehaviorSubject<IUploadProgress>;
    success: BehaviorSubject<IUploadResponse>;
    onCancel: Subject<void>;
}

@Injectable({
    providedIn: 'root'
})
export class UploadTrackerManager {

    trackers: {[uid: string]: IUploadTracker} = {};

    createUploadTracker(file: Dropzone.DropzoneFile, abortFn: () => void): IUploadTrackerHandler {
        let uid: string = Date.now().toString();
        let trackerHandler: IUploadTrackerHandler = {
            uid: uid,
            progress: new BehaviorSubject<IUploadProgress>({percent: 0, bytesSent: 0}),
            success: new BehaviorSubject<IUploadResponse>(null),
            onCancel: new Subject()
        }

        let tracker: IUploadTracker = {
            uid: uid,
            file,
            abort: abortFn,
            progress: trackerHandler.progress.asObservable(),
            success: trackerHandler.success.asObservable(),
            onCancel: trackerHandler.onCancel.asObservable()
        }

        // как закомплитится, удалим из списка
        trackerHandler.progress.subscribe(null, null, () => {
            delete this.trackers[uid];
        });

        this.trackers[uid] = tracker;

        return trackerHandler;
    }

    getTrackerByUid(uid: string) {
        return this.trackers[uid];
    }
}