export interface IUploadConfig {
    acceptedFiles: string;
    url: (file?: Dropzone.DropzoneFile) => string;
    // type: 'image' | 'video' | 'file';
    // allowToFinishAfterDestroy?: boolean; // разрешает закончить загрузку файла даже если директива уже была уничтожена
    maxFileSize?: number;
}