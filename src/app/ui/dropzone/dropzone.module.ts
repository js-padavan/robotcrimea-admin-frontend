import { NgModule } from '@angular/core';
import { DropzoneDirective } from './dropzone.directive';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { FileSizePipe } from './fileSize.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        InlineSVGModule,
        FormsModule
    ],
    declarations: [
        DropzoneDirective,
        FileSizePipe 
    ],
    exports: [ 
        DropzoneDirective, 
        FileSizePipe 
    ]
})
export class DropzoneModule {

}