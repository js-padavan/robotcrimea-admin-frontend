import { Component, HostBinding, Input } from "@angular/core";

export const colors = ['#A7226E', '#EC2049', '#F26B38', '#F7DB4F', '#2F9599', '#1c9bd8', '#00ab50', '#003656'];

export type badgeTypes =  'primary' | 'primaryDark' | 'success' | 'warning' | 'danger' | 'secondary';

@Component({
    selector: 'app-badge',
    templateUrl: './badge.component.html',
    styleUrls: ['./badge.component.scss'],
})
export class BadgeComponent {

    @Input()
    public set groupId(id: number) {
        this.badgeColor = colors[id % colors.length];
    }

    @Input()
    public set type(type: badgeTypes) {
        switch(type) {
            case 'primary': 
                this.badgeColor = '#991acd';
                break;
            case 'success': 
                this.badgeColor = '#57cf93';
                break;
            case 'warning': 
                this.badgeColor = '#ffd740';
                break;
            case 'danger': 
                this.badgeColor = '#be1457';
                break;
            case 'primaryDark':
                this.badgeColor = '#30be1f';
                break;
            case 'secondary':
                this.badgeColor = '#2ea6df';
                break;
        }
    }

    @HostBinding('style.backgroundColor')
    public badgeColor = colors[0];

}