import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, ILoginForm } from '@services/Auth.service';
import { USER_ROLE } from '@services/Users.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  public form: ILoginForm = {
    email: '',
    password: ''
  };
  
  @ViewChild('loginForm', { read: NgForm, static: true })
  private loginForm: NgForm;

  public isOauthInProgress = false;
  public isAuthInProgress = false;
  public authError = false;

  constructor(
    private router: Router,
    private _authService: AuthService,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

  }

  public submit() {
    // @TODO лоадер
    if (!this.loginForm.valid || this.isAuthInProgress) {
      return;
    }

    this.isAuthInProgress = true;
    this._authService.login(this.form)
    .then(() => {
      this.router.navigate(['/'])
      this.isAuthInProgress = false;
      this.authError = false;
    }, () => {
      this.isAuthInProgress = false;
      this.authError = true;
    })
  }
}