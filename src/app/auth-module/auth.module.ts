import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { LoginFormComponent } from './containers/login-form/login-form.component';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        InlineSVGModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        FormsModule
    ],
    declarations: [
        LoginFormComponent
    ],
    exports: [
        LoginFormComponent
    ]
})
export class AuthModule {

}