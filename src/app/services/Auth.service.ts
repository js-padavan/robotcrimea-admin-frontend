import { Injectable, Injector } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import * as jwtDecode from 'jwt-decode';
import { BehaviorSubject } from 'rxjs';
import { Utils } from "./Utils.service";
import { UserService, USER_ROLE } from './Users.service';
import { DateTime } from 'luxon';
import { Router } from '@angular/router';


export interface ILoginResponse {
    access_token: string;
}

export interface ILoginForm {
    email: string;
    password: string
}

export interface ITokenUser {
    email: string;
    exp: number;
    iat: number;
    lastName: string;
    name: string;
    role: USER_ROLE;
    sub: string;
    _id: string;
}


@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public user: ITokenUser = null;
    private _isAuthenticated$$ = new BehaviorSubject(false);
    public isAuthenticated$ = this._isAuthenticated$$.asObservable();

    constructor(
        private _http: HttpClient,
        private _userService: UserService,
        private _injector: Injector,
        private _utils: Utils,
    ) {
    }

    public init(): Promise<void> {
        let token = window.localStorage.getItem('robotcrimea-token');
        if (!token) {
            return Promise.resolve();
        }

        let user: ITokenUser = jwtDecode(token);
        if (DateTime.fromSeconds(user.exp) < DateTime.utc()) {
            return Promise.resolve();
        }
        return this.applyTokenAndLoadUser(token);
    }

    public login(form: ILoginForm) {
        return this._http.post<ILoginResponse>(`/api/auth/login`, form)
                .toPromise()
                .then((response) => {
                    return this.applyTokenAndLoadUser(response.access_token);
                }, (err) => {
                    return Promise.reject(err);
                })

    }

    public async logout() {
        this.user = null;
        this._userService.resetUser();
        this._utils.removeItemFromLocalStorage('robotcrimea-token');
        this._isAuthenticated$$.next(false);
        this._injector.get(Router).navigate(['/login']);
    }

    public applyTokenAndLoadUser(token: string): Promise<void> {
        window.localStorage.setItem('robotcrimea-token', token);
        this.user = jwtDecode(token);
        return this._userService.loadUser()
            .then(() => {
                this._isAuthenticated$$.next(true);
                // if (this._userService.isAdmin()) {
                    
                // } else {
                //     this.logout();
                // }
            }, () => {
                this.logout();
            })
    }
    
    public isAuthenticated(): boolean {
        return this._isAuthenticated$$.value;
    }
}