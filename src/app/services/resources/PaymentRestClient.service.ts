import { Injectable } from "@angular/core";
import { RestClient, IRestFilters } from './RestClient';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

export const filtersConfig: IRestFilters = {
    status: {
        path: 'status',
        type: 'IN'
    },
    type: {
        path: 'type',
        type: 'IN'
    },
    amount: {
        path: 'parentFIO',
        type: 'full'
    },
    createdAt: {
        path: 'childBirthday',
        type: 'BETWEEN'
    },
}

@Injectable({
    providedIn: 'root'
})
export class PaymentRestClient extends RestClient<any, any, any> {

    constructor(
        _httpClient: HttpClient,
        _toastr: ToastrService
    ) {
        super(_httpClient, _toastr);
    }

    path = 'admin/lesson-payments';
    filtersConfig = filtersConfig
}