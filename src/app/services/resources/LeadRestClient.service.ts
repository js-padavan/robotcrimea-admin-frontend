import { Injectable } from "@angular/core";
import { RestClient, IRestFilters } from './RestClient';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ILeadPostForm, ILead } from "../models/lead.model";

export const filtersConfig: IRestFilters = {
    status: {
        path: 'status',
        type: 'IN'
    },
    phone: {
        path: 'phone',
        type: 'substring'
    },
    parentFIO: {
        path: 'parentFIO',
        type: 'substring'
    },
    childBirthday: {
        path: 'childBirthday',
        type: 'BETWEEN'
    },
    branchOffice: {
        path: 'branchOffice',
        type: 'IN'
    },
    comment: {
        path: 'comment',
        type: 'substring'
    },
    trialClass: {
        path: 'trialClass',
        type: 'BETWEEN'
    }
}

export const sortOptions = [
    { value: 'createdAt', label: 'дате создания ↑'},
    { value: '-createdAt', label: 'дате создания ↓'},
    { value: 'childBirthday', label: 'дате рождения ↑'},
    { value: '-childBirthday', label: 'дате рождения ↓'},
    { value: 'updatedAt', label: 'дате изменения ↑'},
    { value: '-updatedAt', label: 'дате изменения ↓'}
]

@Injectable({
    providedIn: 'root'
})
export class LeadRestClient extends RestClient<ILead, ILead, ILeadPostForm> {

    constructor(
        _httpClient: HttpClient,
        _toastr: ToastrService
    ) {
        super(_httpClient, _toastr);
    }

    path = 'admin/lead';
    filtersConfig = filtersConfig;
}