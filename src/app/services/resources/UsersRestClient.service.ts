import { Injectable } from "@angular/core";
import { RestClient, IRestFilters } from './RestClient';
import { HttpClient } from '@angular/common/http';
import { USER_ROLE } from '@services/Users.service';
import { ToastrService } from 'ngx-toastr';

export interface IUser {
    _id: string;
    id: string;
    createdAt: number;
    email: string;
    lastName: string;
    name: string;
    passwordResetHash: string;
    role: USER_ROLE;
}

export interface IUserPostForm {
    email: string;
    name: string;
    lastName: string;
    role: USER_ROLE;
}

export const USER_ROLE_LABELS = {
    [USER_ROLE.ROLE_ADMIN]: {
        label: 'админ',
        badge: ''
    },
    [USER_ROLE.ROLE_MENTOR]: {
        label: 'преподаватель',
        badge: ''
    },
    [USER_ROLE.ROLE_BRANCH_OFFICE_ADMIN]: {
        label: 'админ офиса',
        badge: ''
    },
    [USER_ROLE.ROLE_STUDENT]: {
        label: 'студент',
        badge: ''
    }
}

export const filtersConfig: IRestFilters = {
    role: {
        type: 'IN',
        path: 'role'
    }
}

@Injectable({
    providedIn: 'root'
})
export class UsersRestClient extends RestClient<IUser, IUser, IUserPostForm> {

    constructor(
        _httpClient: HttpClient,
        _toastr: ToastrService
    ) {
        super(_httpClient, _toastr);
    }

    path = 'users';
    filtersConfig = filtersConfig
}