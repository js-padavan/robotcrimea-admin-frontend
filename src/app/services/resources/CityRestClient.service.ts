import { Injectable } from "@angular/core";
import { RestClient, IRestFilters } from './RestClient';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ICity, ICityPostForm } from "../models/dictionary.model";

export const filtersConfig: IRestFilters = {}

@Injectable({
    providedIn: 'root'
})
export class CityRestClient extends RestClient<ICity, ICity, ICityPostForm> {

    constructor(
        _httpClient: HttpClient,
        _toastr: ToastrService
    ) {
        super(_httpClient, _toastr);
    }

    path = 'admin/city';
    filtersConfig = filtersConfig
}