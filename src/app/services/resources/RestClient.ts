
import { HttpClient, HttpParams } from '@angular/common/http';
import { cloneDeep as _cloneDeep } from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

export interface IEntity {
    id: number | string;
}

export interface IQueryResponse<T extends IEntity> {
    docs: T[];
    hasNextPage: number;
    hasPrevPage: number;
    limit: number;
    nextPage: number;
    page: number;
    pagingCounter: number;
    prevPage: number;
    totalDocs: number;
    totalPages: number;
}

export interface IRestFilterConfig {
    type: 'IN' | 'full' | 'substring' | 'BETWEEN' | 'gte' | 'lte';
    path: string;
}

/*
    RestFilters - значение фильтров,
    IRestFilterConfig - конфиг конкретного фильтра (как формировать строку фильтрации), конфиг фильтра будет искаться по ключу
*/

export interface IRestFilters {
    [key: string]: any;
}



export interface IEntityRestParams {
    filters: IRestFilters;
    pagination: {
        page: number;
        pageSize: number;
        totalItems: number;
    },
    sort: string;
}

export const initialEntityRestParams: IEntityRestParams = {
    filters: null,
    pagination: null,
    sort: null
}

// to consider: т.к. у нас есть два интерфейса сущности ModelShort и Model, и надо их как-то различать,
// можно пойти двумя вариантами:
//   - хранить все в одном сторе, но как-то их различать
//   - хранить в двух сторах отдельно, но тогда прийдется дважды обновлятьс сущность, если она находится в двух сторах

// пока что выбрал первый вариант

export abstract class RestClient<ModelShort extends IEntity, Model extends IEntity, PostForm> {

    // store - опциональная привязка к akita EntityStore
    constructor(
        public Http: HttpClient,
        public toastr: ToastrService
    ) {}

    abstract path: string;
    abstract filtersConfig: IRestFilters;

    async query(filters?: IRestFilters, page: number = 1, pageSize: number = 50, sortBy: string = null): Promise<IQueryResponse<ModelShort>> {
        let params = new HttpParams();
        if (page && pageSize) {
            params = params.set('page', page.toString());
            params = params.set('pageSize', pageSize.toString());
        }
        if (sortBy) {
            params = params.set('sort', sortBy);
        }
        if (filters) {
            let queryStr = this._generateQueryString(filters);
            if (queryStr) {
                params = params.set('filters', queryStr);
            }
        }

        return this.Http.get<IQueryResponse<ModelShort>>(`/api/${this.path}`, {params}).toPromise();
    }

    async get(id: string): Promise<Model> {
        return this.Http.get<Model>(`/api/${this.path}/${id}`).toPromise()
    }

    save(form: PostForm): Promise<Model> {
        return  this.Http.post<Model>(`/api/${this.path}`, form)
                .toPromise()
                .then((response) => {
                    this.toastr.success('Успех');
                    return response;
                })
    }

    delete(id: string, form?: any): Promise<void> {
        return  this.Http.delete<void>(`/api/${this.path}/${id}`)
                .toPromise()
                .then((response) => {
                    this.toastr.success('Успешно удален');
                    return response;
                })
    }


    update(id: string, form: Partial<PostForm> ): Promise<Model> {
        return  this.Http.patch<Model>(`/api/${this.path}/${id}`, form)
                .toPromise()
                .then((response) => {
                    this.toastr.success('Успех');
                    return response;
                })
    }

    _generateQueryString(filters: IRestFilters): string {

        let filterObj = Object.keys(filters).reduce((acc, key) => {
            if (!filters[key]) {
                return acc;
            }
            if(this.filtersConfig[key].type === 'BETWEEN' && !filters[key].start) {
                return acc;
            }
            // if (acc) {
            //     acc += '&';
            // }
            switch(this.filtersConfig[key].type) {
                case 'IN': 
                    acc[this.filtersConfig[key].path] = {$in: filters[key]};
                    break;
                case 'full':
                    acc[this.filtersConfig[key].path] = {$full: filters[key]};
                    break;
                case 'substring':
                    acc[this.filtersConfig[key].path] = {$regex: filters[key]};
                    break;
                case 'BETWEEN': 
                    acc["$expr"] = {
                        "$gte": [ 
                            `$${this.filtersConfig[key].path}`, { "$dateFromString": { "dateString": filters[key].start }}
                        ],
                        "$lte": [
                            `$${this.filtersConfig[key].path}`,
                            { "$dateFromString": { "dateString": filters[key].end }}
                        ],
                    }
                    break;
                case 'gte':
                    acc["$expr"] = {
                        "$gte": [
                            `$${this.filtersConfig[key].path}`,
                            { "$dateFromString": { "dateString": filters[key] }},
                        ],
                    };
                    break;
                case 'lte':
                    acc["$expr"] = {
                        "$lte": [
                            `$${this.filtersConfig[key].path}`,
                            { "$dateFromString": { "dateString": filters[key] }},
                        ],
                    };
                    break
            }
            return acc;
        }, {})
        return JSON.stringify(filterObj)
    }
    

}