import { Injectable } from "@angular/core";
import { RestClient, IRestFilters } from './RestClient';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ITrialClassPostForm, ITrialClass } from "../models/lead.model";

export const filtersConfig: IRestFilters = {
    date: {
        type: 'gte',
        path: 'date'
    },
    pastDate: {
        type: 'lte',
        path: 'date'
    }
}

@Injectable({
    providedIn: 'root'
})
export class TrialClassRestClient extends RestClient<ITrialClass, ITrialClass, ITrialClassPostForm> {

    constructor(
        _httpClient: HttpClient,
        _toastr: ToastrService
    ) {
        super(_httpClient, _toastr);
    }

    path = 'admin/trial-class';
    filtersConfig = filtersConfig;
}