import { Injectable } from "@angular/core";
import { RestClient, IRestFilters } from './RestClient';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { IBranchOfficePostForm, IBranchOffice } from "../models/dictionary.model";

export const filtersConfig: IRestFilters = {}

@Injectable({
    providedIn: 'root'
})
export class BranchOfficeRestClient extends RestClient<IBranchOffice, IBranchOffice, IBranchOfficePostForm> {

    constructor(
        _httpClient: HttpClient,
        _toastr: ToastrService
    ) {
        super(_httpClient, _toastr);
    }

    path = 'admin/branch-office';
    filtersConfig = filtersConfig
}