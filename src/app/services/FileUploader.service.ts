import { HttpClient, HttpHeaders, HttpResponse, HttpRequest, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

// если нужно загружать через сервис (когда сожержимое файла уже есть, например из буфера)

@Injectable({
    providedIn: 'root'
})
export class FileUploader {
    constructor(private _http: HttpClient) {

    }

    public uploadFile<T>(file: File, url: string): Observable<HttpEvent<T>> {
        if (!file) {
            return;
        }

        let formData: FormData = new FormData();
        formData.append('file', file, file.name);
        let headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        let request = new HttpRequest('POST', url, formData, {
            headers, 
            reportProgress: true
        })
        return this._http.request<T>(request);
    }
}