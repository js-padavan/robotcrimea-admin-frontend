import { Injectable } from '@angular/core';
import { ParamMap } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class Utils {

    public getToken(): string {
        return window.localStorage.getItem('robotcrimea-token');
    }


    public getItemFromLocalStorage<T>(key: string): T {
        let record = window.localStorage.getItem(key);
        let item: T;
        if (record) {
            try {
                item = JSON.parse(record);
            } catch(e) {
                item = null;
            }
        } else {
            item = null;
        };
        return item;
    }

    public setItemToLocalStorage<T>(key: string, item: T) {
        window.localStorage.setItem(key, JSON.stringify(item));
    }

    public removeItemFromLocalStorage(key: string) {
        window.localStorage.removeItem(key);
    }

    public getIntParam(paramMap: ParamMap, paramName: string): number {
        return paramMap.get(paramName) && paramMap.get(paramName) !== 'new' ? parseInt(paramMap.get(paramName)) : null;
    }
}