import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';



export enum USER_ROLE {
    ROLE_ADMIN = 'ROLE_ADMIN',
    ROLE_BRANCH_OFFICE_ADMIN = 'ROLE_BRANCH_OFFICE_ADMIN',
    ROLE_MENTOR = 'ROLE_MENTOR',
    ROLE_STUDENT = 'ROLE_STUDENT'
}

export interface IUploadResponse {
    _id: string;
    originalname: string;
    encoding: string;
    mimetype: string;
    size: string;
    destination: string;
    filename: string;
    path: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface IUser {
    avatar: IUploadResponse;
    name: string;
    lastName: string;
    email: string;
    role: USER_ROLE;
    createdAt: number;
}


export interface IProfilePostForm {
    name: string;
    lastName: string;
    avatar: string;
}



@Injectable({
    providedIn: 'root'
})
export class UserService {
    private _user$$: BehaviorSubject<IUser> = new BehaviorSubject(null);
    public user$: Observable<IUser> = this._user$$.asObservable();
    public get user() {
        return this._user$$.value;
    }

    constructor(
        private _http: HttpClient
    ) { }

    public resetUser() {
        this._user$$.next(null);
    }

    public loadUser() {
        return this._http.get<IUser>(`/api/profile`)
            .toPromise()
            .then((response) => {
                this._user$$.next(response);
            })
    }

    public async updateProfile(form: IProfilePostForm) {
        let user = await this._http.post<IUser>('/api/users/profile', form).toPromise();
        this._user$$.next(user);
    }

    isMentor() {
        return this.user.role === USER_ROLE.ROLE_MENTOR;
    }

    isBOAdmin() {
        return this.user.role == USER_ROLE.ROLE_BRANCH_OFFICE_ADMIN;
    }

    isAdmin() {
        return this.user.role === USER_ROLE.ROLE_ADMIN;
    }
}