import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './Auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private _authService: AuthService
    ) {

    }
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (!this._authService.isAuthenticated()) {
            this.router.navigate(['login'], {queryParams: {fromPage: state.url}});
            return false;
        }
        return true;
    }
}