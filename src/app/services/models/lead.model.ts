import { IUser } from '@services/resources/UsersRestClient.service';
import { DateTime } from 'luxon';
import { IBranchOffice } from './dictionary.model';

export interface ILeadPostForm {
    parentFIO: string;
    childFIO: string;
    childBirthday: Date | string;
    courseName: string;
    phone: string;
    branchOffice: string;
    comment: string;
    status: string; // LEAD_STATUS
    trialClass: string;
    isNew?: boolean;
}

export interface ITrialClassPostForm {
    mentor: string;
    date: Date | string;
    office: string;
    time?: Date | string;
}

export interface IStatusLabels {
    [status: string]: {
        label: string;
        value?: string;
        badgeType?: string;
    }
}

export enum LEAD_STATUS {
    NEW = 'NEW', // новый
    TRIAL_SCHEDULED = 'TRIAL_SCHEDULED', // назначена дата пробного занятия
    TRIAL_VISITED = 'TRIAL_VISITED', // посетил пробное занятие
    TRIAL_NOT_VISITED = 'TRIAL_NOT_VISITED', // не пришел на пробное занятие
    CONVERTED = 'CONVERTED', // конвертирован в студента
    REJECTED = 'REJECTED', // отклонил предложение
}

export const DIFFRENCE_POINTS_LABELS: IStatusLabels = {
    ['parentFIO']: {
        label: 'ФИО Родителей'
    },
    ['childFIO']: {
        label: 'ФИО ребенка'
    },
    ['childBirthday']: {
        label: 'День рождения ребенка'
    },
    ['courseName']: {
        label: 'Название курса'
    },
    ['phone']: {
        label: 'Телефон'
    },
    ['branchOffice']: {
        label: 'Филиал'
    },
    ['comment']: {
        label: 'Комментарий'
    },
    ['status']: {
        label: 'Статус'
    },
    ['trialClass']: {
        label: 'Пробное занятие'
    }
}

export const LEAD_STATUS_LABELS: IStatusLabels = {
    [LEAD_STATUS.NEW]: {
        label: 'новый',
        value: 'NEW',
        badgeType: 'primary'
    },
    [LEAD_STATUS.TRIAL_SCHEDULED]: {
        label: 'назначена дата занятия',
        value: 'TRIAL_SCHEDULED',
        badgeType: 'secondary'
    },
    [LEAD_STATUS.TRIAL_VISITED]: {
        label: 'посетил занятие',
        value: 'TRIAL_VISITED',
        badgeType: 'success'
    },
    [LEAD_STATUS.TRIAL_NOT_VISITED]: {
        label: 'не пришел на занятие',
        value: 'TRIAL_NOT_VISITED',
        badgeType: 'warning'
    },
    [LEAD_STATUS.CONVERTED]: {
        label: 'конвертирован',
        value: 'CONVERTED',
        badgeType: 'primaryDark'
    },
    [LEAD_STATUS.REJECTED]: {
        label: 'отклонил предложение',
        value: 'REJECTED',
        badgeType: 'danger'
    }
}

export interface ICheckboxValues {
    [key: string] : boolean
}

export const DEFAULT_LEAD_FORM = {
    parentFIO: null,
    childFIO: null,
    childBirthday: DateTime.local().toISODate(),
    courseName: null,
    phone: null,
    branchOffice: null,
    comment: null,
    status: LEAD_STATUS.NEW,
    trialClass: undefined,
}

export const DEFAULT_FILTERS_FORM = {
    parentFIO: null,
    courseName: null,
    status: null,
    childBirthday: {
        start: null,
        end: null
    },
    phone: null,
    comment: null,
    branchOffice: null,
    trialClass: {
        start: null,
        end: null
    }
}

export interface ITrialClass {
    id: string;
    createdAt?: string;
    date?: string;
    leads?: Array<ILead>;
    office: IBranchOffice;
    mentor: IUser;
    updatedAt?: string;
    __v?: number;
    _id?: string;
}

export interface ILead {
    childBirthday: string;
    childFIO: string;
    comment: string;
    branchOffice: IBranchOffice;
    courseName: string;
    createdAt: string;
    id: string;
    parentFIO: string;
    phone: string;
    status: string;
    updatedAt: string;
    trialClass: ITrialClass;
    __v: number;
}