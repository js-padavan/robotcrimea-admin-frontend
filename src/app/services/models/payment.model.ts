import { IUser } from './../resources/UsersRestClient.service';
import { IStatusLabels } from './lead.model';

export enum PAYMENT_TYPE {
    ONLINE = 'online',
    OFFLINE = 'offline'
}

export enum PAYMENT_STATUS {
    PAYMENT_INITED = 'payment_inited',
    PAYMENT_SUCCESS = 'payment_success',
    PAYMENT_FAILED = 'payment_failed'
}

export const PAYMENT_TYPE_LABELS: IStatusLabels = {
    [PAYMENT_TYPE.ONLINE]: {
        label: 'онлайн',
        value: 'ONLINE',
        badgeType: 'secondary'
    },
    [PAYMENT_TYPE.OFFLINE]: {
        label: 'офлайн',
        value: 'OFFLINE',
        badgeType: 'warning'
    }
}

export const PAYMENT_STATUS_LABELS: IStatusLabels = {
    [PAYMENT_STATUS.PAYMENT_INITED]: {
        label: 'обрабатывается',
        value: 'PAYMENT_INITED',
        badgeType: 'primary'
    },
    [PAYMENT_STATUS.PAYMENT_SUCCESS]: {
        label: 'успешно оплачено',
        value: 'PAYMENT_SUCCESS',
        badgeType: 'success'
    },
    [PAYMENT_STATUS.PAYMENT_FAILED]: {
        label: 'оплата не прошла',
        value: 'PAYMENT_FAILED',
        badgeType: 'danger'
    }
}

export interface IPayment {
    amount: number;
    createdAt: string | Date;
    createdBy: IUser;
    group: any;
    id: string;
    invoiceId: string;
    lessonIdxs: number[];
    status: string;
    student: IUser;
    transactionId: null
    type: string;
    updatedAt: string | Date;
    __v: number;
    _id: string;
}

export const DEFAULT_FILTERS_FORM = {
    amount: null,
    createdAt: {
        start: null,
        end: null
    },
    status: null,
    type: null
}