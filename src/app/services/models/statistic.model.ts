export const DEFAULT_CHART_OPTIONS = {
    chart: {
        type: "spline"
     },
     title: {
        text: "История ЗП"
     },
     xAxis:{
        categories:["Jan", "Feb", "Mar", "Apr", "May", "Jun",
           "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
     },
     yAxis: {          
        title:{
           text:"ЗП - рублей"
        } 
     },
     tooltip: {
        valueSuffix:"руб"
     },
     series: [
        {
           name: 'Tokyo',
           data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2,26.5, 23.3, 18.3, 13.9, 9.6]
        }
    ]
}

export const DEFAULT_PIE_CHART_OPTIONS = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Efficiency Optimization by Branch'
    },
    xAxis: {
        categories: [
            'Seattle HQ',
            'San Francisco',
            'Tokyo'
        ]
    },
    yAxis: [{
        min: 0,
        title: {
            text: null
        }
    }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: true,
            shadow: false,
            borderWidth: 1
        }
    },
    series: [{
        name: 'Employees',
        color: 'rgba(165,170,217,1)',
        data: [150, 73, 20],
        pointPadding: 0.3,
        pointPlacement: -0.2
    }, {
        name: 'Employees Optimized',
        color: 'rgba(126,86,134,.9)',
        data: [140, 90, 40],
        pointPadding: 0.4,
        pointPlacement: -0.2
    }]
}

export interface IMentorStats {
    estimatedIncome: {
        [date: string]: number;
    };
    payoutBreakdown: {
        [date: string]: number;
    };
    realIncome: {
        [date: string]: number;
    };
    totalLessons: {
        [date: string]: number;
    };
    workedLessons: {
        [date: string]: number;
    }
}

export interface IDateRange {
   startDate: number;
   endDate: number
}

export interface ISelectOption {
   label: string;
   value?: string;
}