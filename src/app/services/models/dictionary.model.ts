export interface IDictionaryTable {
    name: string;
    id: string;
    createdAt: string;
    city?: string;
}

export interface DialogData {
    name: string;
    value: string;
    office?: string;
}

export interface ICity {
    createdAt: string;
    id: string;
    name: string;
    updatedAt: string;
    __v: number;
    _id: string;
}

export interface ICityPostForm {
    name: string;
}

export interface IBranchOffice {
    address: string;
    createdAt: string;
    id: string;
    name: string;
    updatedAt: string;
    __v: number;
    _id: string;
}

export interface IBranchOfficePostForm {
    name: string;
    address: string;
}