import { Injectable } from '@angular/core';
import { AuthService } from './Auth.service';

@Injectable({
    providedIn: 'root'
})
export class AppBootstrap {
    constructor(
        private _authService: AuthService,
    ) {}


    async init(): Promise<void> {
        await this._authService.init();
        return;
    }

}