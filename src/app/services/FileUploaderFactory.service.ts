import { Injectable } from '@angular/core';
import { IUploadConfig } from '@ui/dropzone';

@Injectable({
    providedIn: 'root'
})
export class FileUploaderFactory {

    constructor(
    ) {

    }

    public getFileUploader(): IUploadConfig {
        return {
            url: () => `/api/admin/lead/upload`,
            acceptedFiles: null
        }
    }
    
}