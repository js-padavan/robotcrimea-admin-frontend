import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { IMentorStats, IDateRange } from "./models/statistic.model";

@Injectable({
    providedIn: 'root'
})
export class MentorStatService {
    constructor(
        private _http: HttpClient
    ) {}

    loadMentorStat(id: string, date: IDateRange) {
        return this._http.get<IMentorStats>(`/api/admin/mentor/stats/${id}?from=${date.startDate}&to=${date.endDate}`)
                    .toPromise();
    }

    loadMentorHistory(id: string) {
        return this._http.get<any>(`/api/admin/lead/${id}/history`)
                    .toPromise();
    }
}