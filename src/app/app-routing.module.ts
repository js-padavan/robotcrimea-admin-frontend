import { Routes } from '@angular/router';
import { AuthGuard } from '@services/AuthGuard.service';
import { AdminLayoutComponent } from './admin-layout/containers/admin-layout.component';
import { LoginFormComponent } from './auth-module/containers/login-form/login-form.component';

export const routes: Routes = [
  {path: 'login', component: LoginFormComponent, children: [
  ]},
  {path: '', component: AdminLayoutComponent, canActivate: [AuthGuard], children: [
    { 
      path: 'statistics', 
      loadChildren: () => import('./statistics-module/statistics.module').then(m => m.StatisticsModule)
    },
    { 
      path: 'dictionary', 
      loadChildren: () => import('./dictionary-module/dictionary.module').then(m => m.DictionaryModule)
    },
    { 
      path: 'lead', 
      loadChildren: () => import('./lead-module/lead.module').then(m => m.LeadModule)
    },
    { 
      path: 'payment', 
      loadChildren: () => import('./payment-module/payment.module').then(m => m.PaymentModule)
    }
  ]}
];
