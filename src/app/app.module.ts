import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { routes } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminLayoutModule } from './admin-layout/admin-layout.module'
import { AuthModule } from './auth-module/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientInterceptor } from './services/HttpInterceptor.service';
import { AppBootstrap } from './services/AppBootstrap.service';
import {MatNativeDateModule} from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ToastrModule } from 'ngx-toastr';

export function appInitializerFactory(appBootstrap: AppBootstrap) {
  return function() {
    return appBootstrap.init();
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AdminLayoutModule,
    RouterModule.forRoot(routes),
    ToastrModule.forRoot(),
    MatNativeDateModule,
    MatDatepickerModule
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: appInitializerFactory, multi: true, deps: [AppBootstrap] },
    { provide: HTTP_INTERCEPTORS, useExisting: HttpClientInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
