import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { OnMentorComponent } from './containers/on-mentor/on-mentor.component';
import { OnBranchComponent } from './containers/on-branch/on-branch.component';
import { OnCityComponent } from './containers/on-city/on-city.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { HighchartsChartModule } from 'highcharts-angular';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        InlineSVGModule,
        MatSelectModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRippleModule,
        HighchartsChartModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatProgressSpinnerModule
    ],
    declarations: [
        OnMentorComponent,
        OnBranchComponent,
        OnCityComponent
    ],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
    ]
})
export class StatisticsModule {

}