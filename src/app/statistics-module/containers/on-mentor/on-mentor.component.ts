import { Component, OnInit } from "@angular/core";
import * as Highcharts from 'highcharts';
import { DateTime } from "luxon";
import { UsersRestClient } from "@services/resources/UsersRestClient.service";
import { MentorStatService } from "@services/MentorStats.service";
import { DEFAULT_CHART_OPTIONS, IDateRange, ISelectOption, IMentorStats, DEFAULT_PIE_CHART_OPTIONS } from '@services/models/statistic.model';
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";
import { cloneDeep as _cloneDeep } from 'lodash';
import { IRestFilters } from '@services/resources/RestClient';

@Component({
    selector: 'on-mentor',
    templateUrl: './on-mentor.component.html',
    styleUrls: ['./on-mentor.component.scss']
})

export class OnMentorComponent implements OnInit {
   public mentorsSelectOpts: ISelectOption[] = [];
   public chartOptions = DEFAULT_CHART_OPTIONS;
   public incomeChartOptions = _cloneDeep(DEFAULT_PIE_CHART_OPTIONS);
   public lessonChartOptions = _cloneDeep(DEFAULT_PIE_CHART_OPTIONS);
   public highcharts = Highcharts;
   public filterForm: FormGroup;
   public mentorStatistic: IMentorStats;
   public loading: boolean = false;
   public defaultFilters: IRestFilters = {role: { type:'IN', path: "ROLE_MENTOR"}};

   constructor(
      private usersRestClient: UsersRestClient,
      private mentorStatService: MentorStatService,
      private formBuilder: FormBuilder
   ) { }

   async ngOnInit() {
      this.filterForm = this.formBuilder.group({
         start: DateTime.local().minus({ days: 1 }).toISODate(),
         end: DateTime.local().toISODate(),
         mentorId: ''
      });
      let users = await this.usersRestClient.query({role: "ROLE_MENTOR"}, null, null);
      this.mentorsSelectOpts = users.docs.map( item => ({value: item._id, label: `${item.name} ${item.lastName}`}));
   }

   public value = 75;
   async loadUsers() {
      this.loading = true;
      if(this.filterForm.valid) {
         let dateToMs: IDateRange = {
            startDate: DateTime.fromJSDate(this.filterForm.value.start).valueOf(),
            endDate: DateTime.fromJSDate(this.filterForm.value.end).valueOf()
         }
         this.mentorStatistic = await this.mentorStatService.loadMentorStat(this.filterForm.value.mentorId, dateToMs);
         this.prepareChart(this.mentorStatistic);
         this.prepareLessonChart(this.mentorStatistic);
         this.prepareIncomeChart(this.mentorStatistic);
         this.loading = false;
      } else {
         return
      }
   }
   
   // потом переделать эти методы, ибо страшные пиздос
   prepareLessonChart(statistic: IMentorStats) {
      this.lessonChartOptions.series = [];
      let workedLessons = {
         color: 'rgba(248,161,63,1)',
         name: 'Отработанных уроков',
         data: _cloneDeep(Object.values(statistic.workedLessons)),
         pointPadding: 0.2,
         pointPlacement: 1
      }
      this.lessonChartOptions.series.push(workedLessons);
      let totalLessons = {
         color: 'rgba(186,60,61,.9)',
         name: 'Всего уроков',
         data: _cloneDeep(Object.values(statistic.totalLessons)),
         pointPadding: 0.2,
         pointPlacement: 1
      }
      this.lessonChartOptions.series.push(totalLessons)
      this.lessonChartOptions.title.text = 'Количество уроков';
      let months = [];
      Object.keys(statistic.payoutBreakdown).map((item) => {
         months.push(DateTime.fromFormat(item, 'dd-MM-yyyy').toFormat('MMM'))
      });
      this.lessonChartOptions.xAxis.categories = _cloneDeep(months);
   }

   prepareIncomeChart(statistic: IMentorStats) {
      this.incomeChartOptions.series = [];
      let realIncome = {
         color: 'rgba(165,170,217,1)',
         name: 'Фактический приход',
         data: _cloneDeep(Object.values(statistic.realIncome)),
         pointPadding: 0.2,
         pointPlacement: 1
      }
      this.incomeChartOptions.series.push(realIncome);
      let estimatedIncome = {
         color: 'rgba(126,86,134,.9)',
         name: 'Ожидаемый приход',
         data: _cloneDeep(Object.values(statistic.estimatedIncome)),
         pointPadding: 0.2,
         pointPlacement: 1
      }
      this.incomeChartOptions.series.push(estimatedIncome)
      this.incomeChartOptions.title.text = 'Приход';
      let months = [];
      Object.keys(statistic.payoutBreakdown).map((item) => {
         months.push(DateTime.fromFormat(item, 'dd-MM-yyyy').toFormat('MMM'))
      });
      this.incomeChartOptions.xAxis.categories = _cloneDeep(months);
   }

   prepareChart(statistic: IMentorStats) {
      this.chartOptions.series[0].name = _cloneDeep(this.mentorsSelectOpts.find(e => e.value === this.filterForm.value.mentorId).label);
      this.chartOptions.series[0].data = _cloneDeep(Object.values(statistic.payoutBreakdown));
      let months = [];
      Object.keys(statistic.payoutBreakdown).map((item) => {
         months.push(DateTime.fromFormat(item, 'dd-MM-yyyy').toFormat('MMM'))
      })
      this.chartOptions.xAxis.categories = _cloneDeep(months);
   }

}