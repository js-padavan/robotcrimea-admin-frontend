import { Route } from '@angular/router';
import { OnMentorComponent } from './containers/on-mentor/on-mentor.component';

export const routes: Route[] = [
    {path: 'on-mentor', component: OnMentorComponent },
]